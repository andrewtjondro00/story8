$(document).ready(function(){
    searchBook("Stress Management");
    $("#searchbook").change(function(){
        searchBook(document.getElementById("searchbook").value);
    });
}); 

function searchBook(toSearch) {
    var searchURL = "https://www.googleapis.com/books/v1/volumes?q=" + toSearch;
    var val = "";
    $.ajax({
        url : searchURL,
        success : function(data){
            $("#result").empty();
            val += "<table class='table table-responsive table-striped' border=1><tr><th style='width: 25%'>Title</th>";
            val += "<th style='width: 25%'>Thumbnail</th>";
            val += "<th style='width: 25%'>Published Date</th>"
            val += "<th style='width: 25%'>Author</th></tr>";
            $.each(data.items, function(i, item){
                val += "<tr><td id='judulBuku'>"+item.volumeInfo.title+"</td>";
                if(item.volumeInfo.imageLinks){
                    if(!item.volumeInfo.imageLinks.smallThumbnail){
                        if(!item.volumeInfo.imageLinks.thumbnail){
                            val += "<td>No thumbnail</td>";
                        }
                        val += "<td><img src=" + item.volumeInfo.imageLinks.thumbnail + "></td>";
                    }
                    else{
                        val += "<td><img src=" + item.volumeInfo.imageLinks.smallThumbnail + "></td>";
                    }
                }
                else{
                    val += "<td>No thumbnail</td>";
                }
                val += "<td>" + item.volumeInfo.publishedDate +  "</td>";
                if(!item.volumeInfo.authors){
                    val += "<td>Tidak ada informasi</td>";
                }
                else{
                    val += "<td>";
                    $.each(item.volumeInfo.authors, function(j, author){
                        val += author + "<br>";
                    });
                    val += "</td>";
                }
            });
            val += "</tr></table>"
            $("#result").append(val);
        },
    });
}
